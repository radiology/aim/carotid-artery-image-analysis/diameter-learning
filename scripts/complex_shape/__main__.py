import random
import argparse
from pathlib import Path

import torch
import cv2
import mlflow
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from monai.transforms import (
    LoadImaged, SpatialPadd, AsChannelFirstd, AddChanneld,
    ToTensord, NormalizeIntensityd 
    )
from diameter_learning.transforms import (
    LoadCarotidChallengeSegmentation, LoadCarotidChallengeAnnotations,
    CropImageCarotidChallenge, PopKeysd, LoadVoxelSized
    )
from diameter_learning.settings import DATA_PRE_PATH
from diameter_learning.apps import CarotidChallengeDataset


if __name__ == '__main__':
    artifact_path: Path = Path(
        mlflow.get_artifact_uri().split('file://')[-1]
    )
    transforms = [
        LoadCarotidChallengeAnnotations("gt")
    ]

    training_dataset: CarotidChallengeDataset = CarotidChallengeDataset(
            transforms=transforms,
            folds=[0],
            cache_rate=0,
            root_dir=DATA_PRE_PATH,
            num_fold=1,
            seed=0,
            annotations=['internal_left', 'internal_right']
        )

    data = []
    for element in training_dataset:
        contour = (
            100 * element['gt_lumen_processed_contour']
            ).astype(int)
        _, (width, height), _ = cv2.minAreaRect(contour)
        aspect_ratio = min(width, height) / max(width, height)
        data.append(
            {
                'slice_id': element['slice_id'],
                'aspect_ratio': aspect_ratio
            }
        )
    dataframe = pd.DataFrame(data).set_index('slice_id')
    dataframe.to_csv(artifact_path / f'aspect_ratio.csv')
