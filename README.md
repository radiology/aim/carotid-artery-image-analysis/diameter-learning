# Diameter learning

![Contribution](resources/image.png)

In this repository, we present a novel methodology that: extracts boundary points from predicted probabilities and use this extraction to optimize a segmentation network based on diameter annotations.

This code refers to the paper [Differentiable Boundary Point Extraction for Weakly Supervised Star-shaped Object Segmentation][openreview-url] accepted and presented in an oral at [MIDL 2022 conference][midl]:

```bibtex
@inproceedings{
    camarasa2022differentiable,
    title={{Differentiable Boundary Point Extraction for Weakly Supervised Star-shaped Object Segmentation}},
    author={Robin Camarasa and Hoel Kervadec and Daniel Bos and Marleen de Bruijne},
    booktitle={Medical Imaging with Deep Learning},
    year={2022},
    url={https://openreview.net/forum?id=whpBn0oadz}
}
```

This implementation relies on the following python libraries:

- [torch][torch-url]: The deep learning backend
- [pytorch-lightning][pytorch-lightning-url]: An overlay over torch that abstracts the mechanics of the training
- [MONAI][monai-url]: The Medical imaging library (Preprocessing transforms, UNet ...)
- [mlflow][mlflow-url]: The logging librairy imaging utilities (Preprocessing transforms, UNet ...)
- The other python libraries used in the project can be found in the requirement file

## Requirements

- Python 3.7
- Conda (optional)

The code has only been tested on [Ubuntu][ubuntu-url], [CentOS][centos-url] and [Arch Linux][arch-url].

## Installation

- For conda users:

``` bash
$ conda env create --name [env-name] --file=conda.yaml
```

- For others:

``` bash
$ pip install -r requirements.txt
```

## Structure

- `CONTRIBUTING.md`: File that set up of the continous integration.
- `MLproject`: File that set up MLexperiments.
- `LICENSE`: File that contains the legal license.
- `diameter_learning/`: Directory that contains the code of this project. the structure is similar to [MONAI][monai-url] project structure to make it easier to contribute.
- `diameter_learning/`: Contains the code of used to compare to baseline. The structure is similar to [MONAI][monai-url] project structure to make it easier to contribute.
- `scripts/`: Directory that contains the entry points of the program.
- `test/`: Directory that contains the tests of the project.
- `test_baselines/`: Directory that contains the tests of the baselines.
- `resources/`: Directory that contains the image(s) of this README file.

## Usage

### Obtain data

You can download the data as a zip archive by joining the [Carotid Artery Vessel Wall Segmentation Challenge](https://vessel-wall-segmentation.grand-challenge.org/Index/). Once downloaded, you can place the data in the right folder by using:

- if you have the zip archive with the data:

```shell
$ make data_zip ZIP_PATH="[your absolute path to the zip archive]"
```

- if you have already inflated the zip archive with the data:

```shell
$ make data_repo REPO_PATH="[your absolute path to inflated folder]"
```

### Preprocess data

- Once you obtained the data you can preprocess them with the command

```shell
$ make preprocess
```

### Run tests

- To ensure the code quality of this project was written using Test Driven Development.

- After preprocessing the data, run the tests in your environment with the following command:
```shell
$ make tests
```

### Launch experiments

- To manage our experiments we use [mlflow][mlflow-url]. [mlflow][mlflow-url] is a library that allows a systematic logging of experiments. A specific experiment is defined in the `MLproject` file via an entry point (and the corresponding parameters). List of the entry points of this project:

    - `stats`: Compute the statistics of the data,
    - `method_training`: Run the training of the method on one fold and one random seed,
    - `method_evaluation`: Run the evaluation of the method on one fold,
    - `geodesic_training`: Run the training of [InExtremIS][inextremis] baseline on one fold and one random seed,
    - `geodesic_evaluation`: Run the evaluation of [InExtremIS][inextremis] baseline on one fold,
    - `full_supervision_training`: Run the training of full supervision baseline on one fold and one random seed,
    - `full_supervision_evaluation`: Run the evaluation of the full supervision baseline on one fold and one random seed,
    - `circle_net_training`: Run the training of [CircleNet][circlenet] baseline on one fold,
    - `circle_net_evaluation`: Run the evaluation of [CircleNet][circlenet] baseline on one fold and one random seed,
    - `aggregate`: Aggregate the results over an experiments,
    - `complex_shape`: Generate the complex shape analysis found in Appendix of the paper.

- Launch an mlflow experiment with conda (`-P param=value` allows to modify the parameters of the experiment)
```shell
$ mlflow run ./ -e [entry-point] --experiment-name [experiment-name]
```

- Launch an mlflow experiment without conda (`-P param=value` allows to modify the parameters of the experiment)
```shell
$ mlflow run ./ -e [entry-point] --experiment-name [experiment-name] --no-conda
```

- Some automation of can also be found in the `Makefile`

### Use the differentiable module in your own code

A minimal working example of the differentiable module is avaible the on [Google colab][google-colab]. The following python code present similar concepts and steps to run the minimal working example locally:

- Within your python3 (virtual) environment of choice launch the following commands:

```bash
git clone https://gitlab.com/radiology/aim/carotid-artery-image-analysis/diameter-learning (fetch)
export PYTHONPATH="$(pwd)/diameter-learning"
pip install -r diameter-learning/requirements.txt
touch example.py
```

- Insert the following python code in `example.py`:

```python
"""Simple example of how to use the differentiable module
"""
import torch
import numpy as np
import matplotlib.pyplot as plt
from diameter_learning.nets.layers import (
    CenterOfMass2DExtractor, VanillaDiameterExtractor,
    MomentGaussianRadiusExtractor
    )


def main():
    """Main function
    """
    # Generate the extractors

    center_of_mass_extractor = CenterOfMass2DExtractor()
    radii_extractor = MomentGaussianRadiusExtractor(moments=[0, 1])
    diameter_extractor = VanillaDiameterExtractor()

    # Create the synthetic example
    x, y = np.mgrid[:50, :50]
    predicted_probabilities = torch.from_numpy(
        1.0 * (((x - 25) / 2) ** 2 + (y - 20) ** 2 < 100)
    ).reshape(1, 1, 50, 50, 1)

    # Apply the extractors to the predicted probabilities
    center_of_mass = center_of_mass_extractor(predicted_probabilities)
    radii = radii_extractor(predicted_probabilities, center_of_mass)
    radii_mean = torch.mean(radii, 0)
    diameters = diameter_extractor(radii_mean)

    # Visualize module
    plt.imshow(predicted_probabilities[0, 0, ..., 0])
    plt.scatter(
        [center_of_mass[0, 0, ..., 0].real],
        [center_of_mass[0, 0, ..., 0].imag]
        )
    angles = np.linspace(-np.pi, np.pi, 25)[:-1]
    plt.scatter(
        radii_mean[0, 0, ..., 0] * np.cos(angles) +\
            center_of_mass[0, 0, ..., 0].real,
        radii_mean[0, 0, ..., 0] * np.sin(angles) +\
            center_of_mass[0, 0, ..., 0].imag,
        color="red"
    )
    plt.title(f"The diameter of this shape is {diameters[0, 0, ..., 0]:.2f}")
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
```

- Run using the following command, and observe the output of the differentiable module
on a synthetic example of predicted probabilities (output of the segmentation network):

```bash
python example.py
```

## To contribute

Please raise an [issue](https://gitlab.com/radiology/aim/carotid-artery-image-analysis/diameter-learning/-/issues) or propose a pull request.

## Authors

The following authors contributed :
- [Robin Camarasa][robin-scholar]
- [Hoel Kervadec][hoel-scholar]
- [Daniel Bos][daniel-scholar]
- [Marleen de Bruijne][marleen-scholar]

[monai-url]: https://github.com/Project-MONAI
[torch-url]: https://pytorch.org/
[mlflow-url]: https://mlflow.org/
[pytorch-lightning-url]: https://www.pytorchlightning.ai/

[centos-url]: https://www.centos.org/
[ubuntu-url]: https://ubuntu.com/
[arch-url]: https://archlinux.org/

[midl]: https://2022.midl.io/papers/i2
[google-colab]: https://colab.research.google.com/drive/1_bN46DMqUVXwV0SeUzXjSztwhwm3-WkB?usp=sharing
[openreview-url]: https://openreview.net/forum?id=whpBn0oadz

[contributing]: https://gitlab.com/radiology/aim/carotid-artery-image-analysis/diameter-learning/-/blob/master/CONTRIBUTING.md

[inextremis]: https://github.com/ReubenDo/InExtremIS
[circlenet]: https://github.com/hrlblab/CircleNet

[robin-scholar]: https://scholar.google.fr/citations?user=-73k3hcAAAAJ&hl=fr
[hoel-scholar]: https://scholar.google.com.hk/citations?user=yeFGhfgAAAAJ&hl=zh-CN
[daniel-scholar]: https://0-scholar-google-com.brum.beds.ac.uk/citations?user=Z40Oy1oAAAAJ&hl=en
[marleen-scholar]: https://scholar.google.hk/citations?user=7kTk3LUAAAAJ

